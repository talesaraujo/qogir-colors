import os, sys
import argparse


COLOR_SCHEMES = {
    'adwaita': ('#E6E0D2', '#B3A78D', '#9A9282', '#696452'),
    'aqua': ('#66A8CB', '#377A9E', '#678EA3', '#355A6E'),
    'elementary': ('#EBD3AD', '#BEA77B', '#B5A991', '#75684D'),
    'faba': ('#DCC5E0', '#CFB8D3', '#FFE9FF', '#A497A5'),
    'mint': ('#92B372', '#8FA876', '#AFCA95', '#779559'),
    'nord': ('#718EAA', '#537494', '#536475', '#384A5C'),
    'skyblue': ('#91AAD2', '#6A88B8', '#5F718C', '#3F5473')
}

DEFAULT_COLOR = ('#6ba4e7', '#5294e2', '#5886bd', '#003479')
DEFAULT_STROKE_VARIANTS = ('#003479', '#003579', '#003779')

BASEDIR = f'{os.getenv("HOME")}/Downloads/Qogir-icon-theme/src'

LOCATIONS = {
    '16': ['devices', 'places'],
    '22': ['places'], 
    '24': ['places'],
    '32': ['apps', 'devices', 'places'],
    '48': ['apps', 'places'],
    '96': ['places'],
    '128': ['places'],
    'scalable': ['apps', 'devices', 'places']
}


def change_color(newcolors):
    for _file in os.listdir():
        with open(_file, 'r') as svgfile:
            content = svgfile.read()

        # Force apply only to blue variant
        if DEFAULT_COLOR[0] in content:
            with open(_file, 'w') as svgfile:
                # First three colors are replaced as usual
                for i in range(3):
                    content = content.replace(DEFAULT_COLOR[i], newcolors[i])
                # Stroke variants (4th color) need to be fixed separately. They all to be
                # replaced by the 4th color of the new pallete.
                for j in range(3):
                    content = content.replace(DEFAULT_STROKE_VARIANTS[j], newcolors[3])
                # Only then svgfile content is overwritten
                svgfile.write(content)


def apply_changes(newcolors):
    os.chdir(BASEDIR)
    
    for folder, subfolders in LOCATIONS.items():
        os.chdir(folder)
        # Here I have access to 'subfolders', which is a list with all subfolder I'd like to go through 
        for subfolder in subfolders:
            os.chdir(subfolder)
            change_color(newcolors)
            os.chdir('..')
        os.chdir('..')

                
def main():

    parser = argparse.ArgumentParser()

    parser.add_argument('-l', '--list', help="List available color variants", action="store_true")
    parser.add_argument('-t', '--theme', help="Select color variant")

    args = parser.parse_args()

    if len(sys.argv) <= 1:
        parser.print_help()
        sys.exit(1)

    if args.list:
        for k, v in COLOR_SCHEMES.items():
            print(f"{k}")

    if args.theme:
        if (args.theme in COLOR_SCHEMES.keys()):
            print("Applying changes...")
            apply_changes(COLOR_SCHEMES[args.theme])
            print("Done.")
        else:
            print("Error: You must provide a valid color scheme.")


if __name__ == "__main__":
    main()
